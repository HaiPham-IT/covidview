import axios from "axios"
import React, { useEffect, useState } from "react"
import DataTable from "react-data-table-component"

const CovidCountriesDataTable = () => {

  const [search, setSearch] = useState("")
  const [records, setRecords] = useState([])
  const [loading, setLoading] = useState(false)
	const [totalRows, setTotalRows] = useState(0)
	const [perPage, setPerPage] = useState(10)
  const [resetToDefaultPage, setResetToDefaultPage] = useState(false)
  const host = process.env.SERVER || 'localhost:8084'

  const fetchRecords = async (page) => {
		setLoading(true)
		const response = await axios.get(`http://${host}/covidcountry/search?searchstr=${search}&pageIndex=${page}&pageSize=${perPage}`)

		setRecords(response.data.list)
		setTotalRows(response.data.total)
		setLoading(false)
	};

  const handlePerRowsChange = async (newPerPage, page) => {
    setLoading(true)

		const response = await axios.get(`http://${host}/covidcountry/search?searchstr=${search}&pageIndex=${page}&pageSize=${newPerPage}`)

		setRecords(response.data.list)
		setPerPage(newPerPage)
		setLoading(false)
  }

  const handlePageChange = (page) => { fetchRecords(page) }

  const columns = [
    {
      name: "Country",
      selector: row => row.country,
      sortable: true
    },
    {
      name: "Continent",
      selector: row => row.continent,
      sortable: true
    },
    {
      name: "Cases",
      selector: row => row.cases
    },
    {
      name: "Deaths",
      selector: row => row.deaths
    },
    {
      name: "Recovered",
      selector: row => row.recovered
    },
    {
      name: "Source",
      selector: row => row.source
    }
  ]

  useEffect(() => { fetchRecords(1) }, [])

  return <DataTable 
  title="CCI"
  columns={columns} 
  data={records}
  progressPending={loading}
  pagination
  paginationServer
  paginationTotalRows={totalRows}
  paginationResetDefaultPage={resetToDefaultPage}
  onChangeRowsPerPage={handlePerRowsChange}
  onChangePage={handlePageChange}
  fixedHeader
  fixedHeaderScrollHeight="450px"
  highlightOnHover
  subHeader
  subHeaderComponent={
  <input 
    type="text" 
    placeholder="Search here" 
    className="w-25 form-control"
    value={search}
    onChange={(e) => setSearch(e.target.value)}
    onKeyDown={e => e.key === 'Enter' && fetchRecords(1) && setResetToDefaultPage(!resetToDefaultPage)}
    />
  }
  />
}

export default CovidCountriesDataTable