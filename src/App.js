import React from "react"
import CovidCountriesDataTable from "./components/CovidCountriesDataTable"
import 'bootstrap/dist/css/bootstrap.min.css'

function App() {
  return (
    <>
      <CovidCountriesDataTable />
    </>
    
  );
}

export default App;
